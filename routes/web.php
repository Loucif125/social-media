<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth routes
Auth::routes();

// Home
Route::get('/home', 'HomeController@index')->name('home');

// Store Post
Route::post('/post/store','PostController@store');

// Store comment
Route::post('/post/comment/store','CommentController@store');

// Store Like
Route::post('/post/like','LikeController@storeLike');

// Filter affichage
Route::post('/home','HomeController@index');
