<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'id', 'id_user', 'id_post',
    ];

    // User function
    public function userComment(){

        return $this->belongsTo('App\User','id_user');
    }
}
