<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model{

    protected $fillable = [
        'id', 'id_user', 'content','image_name','image_url',
    ];

    // User function
    public function userPost(){

        return $this->belongsTo('App\User','id_user');
    }
}
