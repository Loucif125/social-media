<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model{

    protected $fillable = [
        'id', 'id_user', 'id_post',
    ];


    // User function
    public function userLike(){

        return $this->belongsTo('App\User','id_user');
    }

}
