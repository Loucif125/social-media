<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Like;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexEXX(){

        // Show all Post
        $posts= Post::orderBy('id','DESC')->get();
        // Number of posts
        $nbPosts = count($posts);
        // All comment
        $comments = Comment::all();
        // Number comments
        $nbComments = count($comments);
        //Numbers users
        $users = User::all();
        $nbUsers = count($users);
        // Les likes
        $likes = Like::all();


        return view('home',[
            'posts'=>$posts,
            'nbPosts' => $nbPosts,
            'comments' => $comments,
            'nbComments' => $nbComments,
            'nbUsers' => $nbUsers,
            'users' => $users,
            'likes' => $likes
        ]);

    }

    
    // Show post with filtres
    public function index(Request $request){

        // Cas show only post without comment
        if($request->input('filterComment') =='no_comment') {

            $limitPost = $request->input('nb_post');

            // Cas showing posts
            if($request->input('show_image') !="with_image"){
                // Show all Post
                $posts = Post::orderBy('id', 'DESC')->limit($limitPost)->get();
            }

            else{
                // Show all Post with message
                $posts = Post::where('image_url','<>', null)->orderBy('id', 'DESC')->limit($limitPost)->get();
            }


            // Number of posts
            $nbPosts = count($posts);
            // All comment
            $comments = Comment::all();
            // Number comments
            $nbComments = count($comments);
            //Numbers users
            $users = User::all();
            $nbUsers = count($users);
            // Les likes
            $likes = Like::all();

            return view('home', [
                'posts' => $posts,
                'nbPosts' => $nbPosts,
                //'comments' => $comments,
                'nbComments' => $nbComments,
                'nbUsers' => $nbUsers,
                'likes' => $likes,
                'users' => $users,
                'success' =>'Filté avec succès !'
            ]);

        }

        // Cas show only post with comment
       elseif ($request->input('filterComment') =='with_comment') {

           $limitPost = $request->input('nb_post');

           // Show all Post
           $posts = Post::orderBy('id', 'DESC')->limit($limitPost)->get();
           // Number of posts
           $nbPosts = count($posts);
           // All comment
           $comments = Comment::all();
           // Number comments
           $nbComments = count($comments);
           //Numbers users
           $users = User::all();
           $nbUsers = count($users);
           // Les likes
           $likes = Like::all();

           return view('home', [
               'posts' => $posts,
               'nbPosts' => $nbPosts,
               'comments' => $comments,
               'nbComments' => $nbComments,
               'nbUsers' => $nbUsers,
               'likes' => $likes,
               'users' => $users,
               'success' =>'Filté avec succès !'
           ]);
       }


        // No request filter
        else{
            // Show all Post
            $posts = Post::orderBy('id', 'DESC')->get();
            // Number of posts
            $nbPosts = count($posts);
            // All comment
            $comments = Comment::all();
            // Number comments
            $nbComments = count($comments);
            //Numbers users
            $users = User::all();
            $nbUsers = count($users);
            // Les likes
            $likes = Like::all();

            return view('home', [
                'posts' => $posts,
                'nbPosts' => $nbPosts,
                'comments' => $comments,
                'nbComments' => $nbComments,
                'nbUsers' => $nbUsers,
                'users' => $users,
                'likes' => $likes

            ]);

        }


    }


}
