<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LikeController extends Controller{


    public function storeLike(Request $request){

        $like = new Like();
        $like->id_user = $request->input('id_user');
        $like->id_post = $request->input('id_post');
        $like->save();

        return redirect()->back()->with('success','Like ajouté avec succès !');
    }
}
