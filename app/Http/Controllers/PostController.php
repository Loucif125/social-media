<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller{


    public function store(Request $request){

        $post = new Post();
        $post->id_user = $request->input('id_user');
        $post->content = $request->input('content');


        if ($files = $request->file('image')) {

            $destinationPath = 'public/images/'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $post->image_name = "$profileImage";
            $post->image_url = $destinationPath.''.$profileImage;

        }

        $post->save();

        return redirect()->back()->with('success','Post ajouté avec succès !');
    }

}
