<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller{

    public function store(Request $request){

        $comment = new Comment();
        $comment->id_post = $request->input('id_post');
        $comment->id_user = $request->input('id_user');
        $comment->comment = $request->input('comment');
        $comment->save();

        return redirect()->back()->with('success','Ajouté avec succès !');

    }

}
