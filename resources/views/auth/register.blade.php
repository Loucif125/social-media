@extends('layouts.app')

@section('content')

<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4"> Inscription </h1>
                        </div>
                        <form class="user" method="post" action="{{ route('register') }}">
                            @csrf

                            @if ($errors->has('name'))
                            <div class="form-group">
                                <input type="text" name="email" class="form-control form-control-user" id="exampleFirstName" placeholder="Name" value="{{old('name')}}">
                                <span>{{ $errors->first('name') }}</span>
                            </div>
                            @else
                                <div class="form-group">
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control form-control-user" id="exampleFirstName" placeholder="Name">
                                </div>
                            @endif


                            @if ($errors->has('email'))
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address">
                                    <span>{{ $errors->first('email') }}</span>
                                </div>
                            @else
                                <div class="form-group">
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email">
                                </div>
                            @endif


                            <div class="form-group row">
                                @if ($errors->has('email'))
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                    <span>{{ $errors->first('password') }}</span>
                                </div>
                                @else
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                    </div>
                                @endif

                                <div class="col-sm-6">
                                    <input type="password" name="password_confirmation" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Confirmation">
                                </div>
                            </div>

                            <button class="btn btn-primary btn-user btn-block"> S'incrire </button>
                        </form>

                        <hr>
                        <div class="text-center">
                            <a class="small" href="login.html"> Déjà inscrit! Ici connexion </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Bootstrap core JavaScript-->
<script src="sm/vendor/jquery/jquery.min.js"></script>
<script src="sm/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="sm/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="sm/js/sb-admin-2.min.js"></script>

</body>





@endsection
