@extends('layouts.app')

@section('content')


    <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous">

    </script>


@if(session('success') or isset($success))
    <div class="alert alert-success" style="text-align: center" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong><i class="fa fa-comment"></i> {{session('success')}}</strong>
    </div>

    <script>
        $(document).ready(function() {
            $("#success-alert").hide();

                $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#success-alert").slideUp(500);
                });

        });
    </script>
    @endif


<div id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/home')}}">
    <div class="sidebar-brand-icon-alt">
        <i class="fas fa-sort-amount-down-alt fa-2x"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Menu <sup>(Filtre)</sup></div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item active ">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-cog"></i>
        <span>Trier l'affichage</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">

            <form method="post" action="{{ url('/home')}}" >
                @csrf
                <div class="collapse-item"><input type="checkbox" name="show_image" id="image" value="with_image" checked/> <label for="image"> Image</label></div>
                <div class="collapse-item"><input type="checkbox" name="message" id="message" checked/> <label for="message"> Messages</label></div>
                <div class="collapse-item"><input type="radio" name="filterComment" id="with_comment"  value="with_comment" checked/> <label for="with_comment"> Avec commentaire</label></div>
                <div class="collapse-item"><input type="radio" name="filterComment" id="no_comment" value="no_comment"/> <label for="no_comment"> Sans commentaire</label></div>
                <div class="collapse-item">
                    <select id="nb_posts" name="nb_post">
                        @for($i =1; $i< $nbPosts; $i++ )
                        <option value="{{$i}}">{{$i}}</option>
                        @endfor
                        <option value="{{$nbPosts}}" selected>{{$nbPosts}}</option>
                    </select>

                    <label for="nb_posts"> Nombre de posts</label>
                </div>

                <div class="collapse-item"><button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Afficher</button></div>
            </form>

        </div>
    </div>
</li>


<!-- Divider -->
<hr class="sidebar-divider">

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

    <!-- Begin Page Content -->
    <div class="container-fluid mt-2">
        <!-- Content Row -->
        <div class="row">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4 ">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Utilisateur(s)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{($nbUsers == 0 ? "0": $nbUsers)}}

                                    <button type="button" data-toggle="modal" data-target="#infos" class="btn btn-primary btn-sm"><i class="fa fa-clipboard-list"></i> Liste</button>
                                </div>

                                    




                            </div>
                            <div class="col-auto">
                                <i class="fas fa-users fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Post(s)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{($nbPosts == 0 ? "0": $nbPosts)}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Commentaire</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{($nbComments == 0 ? "0": $nbComments)}}</div>

                            </div>
                            <div class="col-auto">
                                <i class="fas fa-clipboard-list fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Nouveaux Users</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{($nbUsers == 0 ? "0": $nbUsers)}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-user fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content Row -->



    </div>
    <!-- /.container-fluid -->


    <!-- Poster un message--->

    <div class="containrer-fluid">
        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-9">
                    <div class="card o-hidden border-0 shadow-lg my-3">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="p-4">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4"> <i class="fa fa-comments"></i> Ajouter un Post :</h1>
                                        </div>
                                        <form class="user" method="POST" action="{{url('/post/store')}}" enctype="multipart/form-data">
                                            @csrf

                                            <div class="form-group">
                                                <textarea name="content" class="form-control form-control-user" required placeholder="Quoi de neuf aujourd'hui ?">
                                                </textarea>
                                            </div>

                                            <div class="form-group">
                                                Image: <input type="file" name="image" id="exampleInputName">
                                            </div>

                                            <input type="hidden" name="id_user" value="@if(Auth::user()->id != null) {{Auth::user()->id}} @endif" />
                                            <button type="submit" class="btn btn-primary btn-user btn-block"><b><i class="fas fa-comment"></i> Poster</b></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <!-- Fin poster un message -->





    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-comments"></i> File d'actualités </h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <!--- Start Test-->
            @foreach($posts as $post)
            <div class="container">
                    <!-- Outer Row -->
                    <div class="row justify-content-center">
                        <div class="col-xl-10 col-lg-12 col-md-9">
                            <div class="card o-hidden border-0 shadow-lg my-2">
                                <div class="card-body p-0">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-3">
                                                <div>
                                                    <h1 class="h4 text-gray-900 mb-4"> <i class="fa fa-user "></i> {{ucfirst($post->userPost->name)}} <span class="float-lg-right">
                                                            <span style="font-size: 0.6em">Posté le : {{$post->created_at}}</span></span>

                                                    </h1>
                                                </div>
                                                <div class="form-group">
                                                    <div style="border: 0px solid black; border-radius: 4px; background-color: rgba(0,0,0,0.05); padding: 5px">
                                                        <p class="p-2">{{ucfirst($post->content)}}</p>

                                                        @if($post->image_url != "")
                                                        <div class="text-center bg-gray-400">
                                                            <img src="{{asset($post->image_url)}}" style="height: 160px"/>
                                                        </div>
                                                        @endif
                                                    </div>

                                                    <form class="user p-1"  method="post" action="{{url('/post/like')}}">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input type="hidden" name="id_post"  value="{{$post->id}}"/>
                                                            <input type="hidden" name="id_user" value="{{Auth::user()->id}}"/>
                                                            @if(isset($likes))
                                                            @php 
                                                                $bool = false;
                                                            @endphp

                                                                @foreach($likes as $like)
                                                                    @if(($like->id_post == $post->id) and ($like->id_user == Auth::user()->id))
                                                                    
                                                                        @php
                                                                        $bool = true;
                                                                        @endphp
                                                                    @endif
                                                        
                                                                @endforeach

                                                                @if($bool ==true)
                                                                <button type="submit" class="btn btn-primary" disabled><i class="fa fa-thumbs-up"></i> Liker</button>
                                                                @else
                                                                <button type="submit" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Liker</button>
                                                                @endif
                                                            @endif



                                                            <div class="float-right">
                                                                Like(s):
                                                                @if(isset($likes))
                                                                    @foreach($likes as $like)
                                                                        @if($like->id_post == $post->id)
                                                                        {{ucwords($like->userLike->name).';'}}
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </div>


                                                        </div>

                                                    </form>


                                                </div>

                                                <div class="pl-5">
                                                    @if(isset($comments) and count($comments) !=0)
                                                        @foreach($comments as $comment)
                                                            @if($comment->id_post == $post->id)

                                                                <div class="form-group">
                                                                    <p><b>{{$comment->userComment->name}} : </b> {{$comment->comment}} <span class="float-lg-right"> <span style="font-size: 0.6em">Le : {{$comment->created_at}}</span></span></p>
                                                                </div>

                                                            @endif
                                                        @endforeach
                                                    @endif



                                                    <form class="user" method="post" action="{{url('/post/comment/store')}}">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input type="text" class="form-control form-control-user" name="comment" required placeholder="Commentaire"/>
                                                            <input type="hidden" name="id_post"  value="{{$post->id}}"/>
                                                            <input type="hidden" name="id_user" value="{{Auth::user()->id}}"/>
                                                        </div>

                                                    <button type="submit" class="btn btn-success"><i class="fa fa-comments"></i> Commenter</button>
                                                    </form>



                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                    @endforeach
                    <!-- End Test-->


                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->



</div>
<!-- End of Main Content -->











<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Média Social</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>








<!-- Modal-->

                                    
                                @if(isset($users))
                                <div class="modal" id="infos">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h4 class="modal-title"><i class="fa fa-users"></i> Liste des utilisateurs</h4>
                                            <button type="button" class="close" data-dismiss="modal">
                                              <span>&times;</span>
                                            </button>            
                                          </div>
                                          <div class="modal-body">
                                            <ul>
                                                @foreach($users as $user)
                                                <li> <i class="fa fa-user"></i> {{ucwords($user->name)}}</li>
                                                @endforeach

                                            </ul>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                                          </div>
                                        </div>
                                      </div>
                                  @endif

<!-- Fin Modal --->



@endsection
